﻿using System;
using System.Collections.Generic;
using System.Text;
using projectLibrary.Interface;
using projectLibrary.Services;

namespace projectLibrary.Entity
{
    class User:Base,IUser
    {
        private static int _autoIncreatment = 0;
        private static List<User>_userList;
        private string _password;
        private bool _status;

        public string Password { get => _password; set => _password = value; }
        public static List<User> UserList { get => _userList; }
        public bool Status { get => _status; set => _status = value; }


        /// <summary>
        ///khởi tạo 1 user
        /// </summary>
        /// <param name="name">tên user</param>
        /// <param name="password">mật khẩu user</param>
        public User(string name="", string password="", int? id= null):base(name:name)
        {
            if (string.IsNullOrEmpty(password))
            {
                throw new Exception("loi nhap mat khau!");
            }
            
            if (_userList == null)
            {
                _userList = new List<User>();
            }

            var user = User.UserList.Find(x => x.Name == name);
            if (user != null)
            {
                throw new Exception("da ton tai nguoi dung co ten la:" + name);
            }

            base._id =id.HasValue?id.Value: ++_autoIncreatment; 
            this._password = password;
            this._status = true;
            base.Name = name;
            _userList.Add(this);
        }
        /// <summary>
        /// tao du lieu de ghi
        /// </summary>
        /// <returns></returns>
        public static string[] ToFile()
        {
            List<string> _dataUser = new List<string>();
            _dataUser.Add($"{_autoIncreatment}");
            if (_userList != null)
            {
                for (int i = 0; i < _userList.Count; i++)
                {
                    _dataUser.Add($"{_userList[i].Id}###{_userList[i].Name}###{_userList[i].Password}###{_userList[i].Status}");
                }
            }
            //add o day la add cac phan tu cua list a
            return _dataUser.ToArray();
        }

        /// <summary>
        /// ghi file
        /// </summary>
        /// <param name="path"></param>
        public static void WriteFile(string path)
        {
            string[] arrString = ToFile();
            IOFIle.WriteFile(path, arrString);

        }

        /// <summary>
        /// doc file
        /// </summary>
        /// <param name="path"></param>
      

        public static void ReadFile(string path)
        {
            string[] arr = IOFIle.ReadFile(path);

            int.TryParse(arr[0], out _autoIncreatment);
            Author.ListAuthor.Clear();
            for (int i = 1; i < arr.Length; i++)
            {
                string[] arr2 = arr[i].Split("###");
                Author.ListAuthor.Add(new Author(arr2[0], Convert.ToInt32(arr2[1])));
            }

        }

    }
}
