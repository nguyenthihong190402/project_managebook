﻿using System;
using System.Collections.Generic;
using System.Text;
using projectLibrary.Interface;
using projectLibrary.Services;

namespace projectLibrary.Entity
{
    class Reader : Base, IReader
    {
        private static List<Reader> _readersList;
        private List<Card> _cardsList;
        private static int _autoIncreatment = 0;
        private DateTime _registrationDate;

        public DateTime RegistrationDate { get => _registrationDate; }
        public static List<Reader> ReaderList { get => _readersList; }
        public List<Card> ReaderCardList { get => _cardsList; }

        /// <summary>
        /// khỏi tạo 1 bạn đọc
        /// </summary>
        /// <param name="name">tên bạn đọc</param>
        /// <param name="registrationDate">ngày đăng ký</param>
        /// <param name="id">id của người đọc xác định đã có giá trị chưa</param>
        public Reader(string name = "", string registrationDate = null, int? id = null) : base(name: name)
        {
            if (_readersList == null)
            {
                _readersList = new List<Reader>();
            }
            try
            {
                this._registrationDate = registrationDate == null ? DateTime.Now : Convert.ToDateTime(registrationDate);
            }
            catch (Exception)
            {

                throw new Exception("loi dinh dang ngay thang nam yeu cau nhap [dd/MM/yyyy]");
            }
           
            base._id = id.HasValue ? id.Value : ++_autoIncreatment;
            base.Name = name;
            this._cardsList = new List<Card>();
            _readersList.Add(this);
        }

        /// <summary>
        /// lấy dữ liệu cần ghi vào file
        /// </summary>
        /// <returns>chuỗi dữ liệu để ghi vào file</returns>
        public static string[] ToFile()
        {
            List<string> dataOfReader = new List<string>();
            dataOfReader.Add($"{_autoIncreatment}");

            if (_readersList != null)
            {
                for (int i = 0; i < Reader.ReaderList.Count; i++)
                {
                    dataOfReader.Add($"{_readersList[i].Id}" +
                        $"###{_readersList[i].Name}" +
                        $"###{_readersList[i].RegistrationDate.ToShortDateString()}");
                }
            }

            //add o day la add cac phan tu cua list a
            return dataOfReader.ToArray();
        }

        public static void WriteFile(string path)
        {
            string[] arrString = ToFile();
            IOFIle.WriteFile(path, arrString);

        }

        public static void ReadFile(string path)
        {
            string[] arr = IOFIle.ReadFile(path);
        }
    }
}
