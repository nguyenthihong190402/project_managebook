﻿using projectLibrary.Entity;
using System;
using System.Collections.Generic;

namespace projectLibrary.Services
{
    class Screen
    {
        /// <summary>
        /// Man hinh dang nhap
        /// </summary>
        public static void Login()
        {
            //nhập userName và password
            string userName;
            string password;

            //kiểm tra userName và password
            //var isLoggedIn;

            //kiem tra khoa tai khoan
            bool islockIn;

            //số lần đăng nhập thất bại
            List<string> loggedCountFail = new List<string>();

            var itemMenu = new List<IKeyValue>();
            itemMenu.Add(new KeyValue<string>(key: "Nhap user", value: string.Empty));
            itemMenu.Add(new KeyValue<string>(key: "Nhap password", value: string.Empty, isPassword: true));
            do
            {
                View.HeaderTitle("Dang nhap he thong");
                View.ReadLines(itemMenu);
                userName = (string)itemMenu[0].GetValue();
                password = (string)itemMenu[1].GetValue();
                loggedCountFail.Add(userName);

               var isLoggedIn = Function.Login(username: userName, passworld: password);
                if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                {
                    View.Warning("Ten va mat khau khong duoc trong");
                }
                else
                {
                    if (isLoggedIn!=null)
                    {
                        //Thuc hien cac chuc nang
                        Menu();
                        Function.GetAllData();
                        return;
                    }
                    else
                    {
                        View.Warning("Ten dang nhap hoac mat khau khong dung");
                        if (loggedCountFail.FindAll(item => item == userName).Count == 3)
                        {
                            islockIn = Function.LockLogin(userName: userName);
                            if (islockIn)
                            {
                                View.Warning($"Tai khoan {userName} da bi khoa");
                                loggedCountFail = new List<string>();
                            }
                        }
                    }
                }
            } while (true);
        }

        private static void Menu()
        {
            var item = new List<IKeyValue>();
            item.Add(new KeyValue<string>(key: "1", value: "Quan ly sach"));
            item.Add(new KeyValue<string>(key: "2", value: "Quan ly phieu muon"));
            item.Add(new KeyValue<string>(key: "3", value: "Quay lai dang nhap"));
            item.Add(new KeyValue<string>(key: "ESC", value: "Thoat khoi chuong trinh"));


            char luaChon = '\0';
            do
            {
                View.HeaderTitle("Lua chon chuc nang chuong trinh");
                View.WriteLines(item);
                var input = new List<IKeyValue>();
                input.Add(new KeyValue<char>(key: "Nhap lua chon"));
                View.ReadLines(input);
                luaChon = (char)input[0].GetValue();


            } while (luaChon != '1' && luaChon != '2' && luaChon != '3' && luaChon != (char)ConsoleKey.Escape);

            switch (luaChon)
            {
                case '1':
                    MenuManageBook();
                    break;
                case '2':
                    MenuManageCard();
                    break;
                case '3':
                    Login();
                    break;
            }
        }
        /// <summary>
        /// Menu quan ly sach
        /// Hien thi thong tin sach
        /// Them sach vao 
        /// Xoa sach
        /// </summary>
        private static void MenuManageBook()
        {
            View.HeaderTitle("Menu quan ly sach");
            var itemMenu = new List<IKeyValue>();
            itemMenu.Add(new KeyValue<string>(key: "1", value: "Hien thi thong tin sach"));
            itemMenu.Add(new KeyValue<string>(key: "2", value: "Them sach"));
            itemMenu.Add(new KeyValue<string>(key: "3", value: "Xoa sach"));
            itemMenu.Add(new KeyValue<string>(key: "4", value: "Quay lai menu chinh"));
            itemMenu.Add(new KeyValue<string>(key: "ESC", value: "Thoat khoi chuong trinh"));

            char luaChon = '\0';
            do
            {
                View.WriteLines(itemMenu);
                var inputItem = new Dictionary<string, IKeyValue>();
                inputItem.Add("Nhap lua chon", new KeyValue<char>(key: "Nhap lua chon"));
                View.ReadLines(inputItem);
                luaChon = (char)inputItem["Nhap lua chon"].GetValue();
            } while (luaChon != '1' && luaChon != '2' && luaChon != '3' && luaChon != '4' && luaChon != (char)ConsoleKey.Escape);

            switch (luaChon)
            {
                case '1':
                    HienThiThongTinSach();
                    break;
                case '2':
                    ThemSach();
                    break;
                case '3':
                    XoaSach();
                    break;
                case '4':
                    Menu();
                    break;
            }
        }
        /// <summary>
        /// Menu quan ly phieu muon
        /// Hien thi thong tin phieu muon
        /// Muon sach 
        /// Tra sach
        /// </summary>
        private static void MenuManageCard()
        {
            View.HeaderTitle("Menu quan ly phieu muon");
            var itemMenu = new List<IKeyValue>();
            itemMenu.Add(new KeyValue<string>(key: "1", value: "Hien thi thong tin phieu muon"));
            itemMenu.Add(new KeyValue<string>(key: "2", value: "Muon sach"));
            itemMenu.Add(new KeyValue<string>(key: "3", value: "Tra sach"));
            itemMenu.Add(new KeyValue<string>(key: "4", value: "Quay lai menu chinh"));
            itemMenu.Add(new KeyValue<string>(key: "ESC", value: "Thoat khoi chuong trinh"));


            char luaChon = '\0';
            do
            {
                View.WriteLines(itemMenu);
                var inputItem = new Dictionary<string, IKeyValue>();
                inputItem.Add("Nhap lua chon", new KeyValue<char>(key: "Nhap lua chon"));
                View.ReadLines(inputItem);
                luaChon = (char)inputItem["Nhap lua chon"].GetValue();
            } while (luaChon != '1' && luaChon != '2' && luaChon != '3' && luaChon != '4' && luaChon != (char)ConsoleKey.Escape);

            switch (luaChon)
            {
                case '1':
                    HienThiThongTinPhieuMuon();
                    break;
                case '2':
                    MuonSach();
                    break;
                case '3':
                    TraSach();
                    break;
                case '4':
                    Menu();
                    break;
            }
        }

        private static void HienThiThongTinSach()
        {
            View.HeaderTitle("Hien thi thong tin sach");
            List<IKeyValue> showBook;
            List<Book> book = Book.BooksList;
            showBook = ParseBook(booksList: book);
            View.WriteLines(showBook);
            Console.ReadKey();

            MenuManageBook();

        }


        private static void HienThiThongTinPhieuMuon()
        {
            View.HeaderTitle("Hien thi thong tin phieu muon");
            List<IKeyValue> showCard;
            List<Card> card = Card.CardList;
            showCard = ParseCard(cardsList: card);
            View.WriteLines(showCard);
            Console.ReadKey();

            MenuManageCard();
        }

        private static void ThemSach()
        {
            View.HeaderTitle("Them sach");
            //int bookId;
            string bookName;
            int price;
            int releaseYear;
            int pageNumber;
            string inputDate;
            int authorId;
            int publisherId;

            Dictionary<string, IKeyValue> inputBook = new Dictionary<string, IKeyValue>();
            inputBook.Add("Nhap ten sach", new KeyValue<string>(key: "Nhap ten sach", value: string.Empty));
            inputBook.Add("Nhap gia sach", new KeyValue<int>(key: "Nhap gia sach"));
            inputBook.Add("Nhap nam phat hanh sach", new KeyValue<int>(key: "Nhap nam phat hanh sach"));
            inputBook.Add("Nhap so trang sach", new KeyValue<int>(key: "Nhap so trang sach"));
            inputBook.Add("Nhap ngay nhap sach", new KeyValue<string>(key: "Nhap ngay nhap sach", value: string.Empty));
            inputBook.Add("Nhap ma tac gia", new KeyValue<int>(key: "Nhap ma tac gia"));
            inputBook.Add("Nhap ma nha xuat ban", new KeyValue<int>(key: "Nhap ma nha xuat ban"));

            View.ReadLines(inputBook);
            bookName = (string)inputBook["Nhap ten sach"].GetValue();
            price = (int)inputBook["Nhap gia sach"].GetValue();
            releaseYear = (int)inputBook["Nhap nam phat hanh sach"].GetValue();
            pageNumber = (int)inputBook["Nhap so trang sach"].GetValue();
            inputDate = (string)inputBook["Nhap ngay nhap sach"].GetValue();
            authorId = (int)inputBook["Nhap ma tac gia"].GetValue();
            publisherId = (int)inputBook["Nhap ma nha xuat ban"].GetValue();

            try
            {
                var book = Function.BookAdd(name: bookName, price: price, releaseYear: releaseYear, numPage: pageNumber, inputDate: inputDate, authorId: authorId, publisherId: publisherId);
                if (book != null)
                {
                    View.Success($"Them sach thanh cong ten sach la : {book.Name}");
                }
                else
                {
                    View.Warning("Them sach that bai");
                }
            }
            catch (Exception e)
            {

                View.Warning(e.Message);
            }

            MenuManageBook();
        }

        private static void XoaSach()
        {
            View.HeaderTitle("Xoa sach");
            int bookId;
            Dictionary<string, IKeyValue> inputBook = new Dictionary<string, IKeyValue>();
            inputBook.Add("Nhap ma sach", new KeyValue<int>(key: "Nhap ma sach"));

            View.ReadLines(inputBook);
            bookId = (int)inputBook["Nhap ma sach"].GetValue();

            try
            {
                var book = Function.BookDelete(BookId: bookId);
                if (book == true)
                {
                    View.Success($"Xoa sach co id la {bookId} thanh cong");
                }
                else
                {
                    View.Warning("Xoa sach khong thanh cong");
                }
            }
            catch (Exception e)
            {

                View.Warning(e.Message);
            }

            Function.SetAllData();
            MenuManageBook();
        }

        private static void MuonSach()
        {
            View.HeaderTitle("Muon sach");
            int bookId;
            int readerId;
            Dictionary<string, IKeyValue> inputBook = new Dictionary<string, IKeyValue>();
            inputBook.Add("Nhap ma sach", new KeyValue<int>(key: "Nhap ma sach"));
            inputBook.Add("Nhap ma ban doc", new KeyValue<int>(key: "Nhap ma ban doc"));

            View.ReadLines(inputBook);
            bookId = (int)inputBook["Nhap ma sach"].GetValue();
            readerId = (int)inputBook["Nhap ma ban doc"].GetValue();

            try
            {
                var book = Function.CardAdd(bookId: bookId, ReaderId: readerId);
                if(book != null)
                {
                    View.Success($"Muon sach thanh cong");
                }
                else
                {
                    View.Warning("Muon sach that bai");
                }
            }
            catch (Exception e)
            {
                View.Warning(e.Message);
            }
            Function.SetAllData();
            MenuManageCard();
        }

        private static void TraSach()
        {
            View.HeaderTitle("Tra sach");
            int cardNumber;
            Dictionary<string, IKeyValue> inputBook = new Dictionary<string, IKeyValue>();
            inputBook.Add("Nhap ma phieu muon", new KeyValue<int>(key: "Nhap ma phieu muon"));

            View.ReadLines(inputBook);
            cardNumber = (int)inputBook["Nhap ma phieu muon"].GetValue();

            try
            {
                var book = Function.CardReturn(cardId: cardNumber);
                if (book == true)
                {
                    View.Success($"Tra sach thanh cong");
                }
                else
                {
                    View.Warning("Tra sach that bai");
                }
            }
            catch (Exception e)
            {
                View.Warning(e.Message);
            }
            Function.SetAllData();
            MenuManageCard();
        }

        private static List<IKeyValue> ParseCard(List<Card> cardsList)
        {
            List<IKeyValue> result = new List<IKeyValue>();
            int i = 0;
            foreach (var item in cardsList)
            {
                result.Add(new KeyValue<string>(key: $"{++i}--------{i}", value: $"{i}-----------{i}"));
                result.Add(new KeyValue<string>(key: "So phieu muon", value: $"{item.NumberCard}"));
                result.Add(new KeyValue<string>(key: "Ma phieu muon", value: $"{item.Id}"));
                result.Add(new KeyValue<string>(key: "Tinh trang", value: $"{item.Status}"));
                result.Add(new KeyValue<string>(key: "Ten sach", value: $"{item.BookCard.Name}"));
                result.Add(new KeyValue<string>(key: "Ten ban doc", value: $"{item.ReaderCard.Name}"));
                result.Add(new KeyValue<string>(key: "Ngay muon sach", value: $"{item.BookLoanDate}"));
                result.Add(new KeyValue<string>(key: "Ngay tra sach", value: $"{item.BookReturnDate}"));
            }

            return result;
        }
        private static List<IKeyValue> ParseBook(List<Book> booksList)
        {
            List<IKeyValue> result = new List<IKeyValue>();
            int i = 0;
            foreach (var item in booksList)
            {
                result.Add(new KeyValue<string>(key: $"{++i}-----------{i}", value: $"{i}----------{i}"));
                result.Add(new KeyValue<string>(key: "Ma sach", value: $"{item.Id}"));
                result.Add(new KeyValue<string>(key: "Ten sach", value: $"{item.Name}"));
                result.Add(new KeyValue<string>(key: "Gia sach", value: $"{item.Price}"));
                result.Add(new KeyValue<string>(key: "Nam phat hanh", value: $"{item.ReleaseYear}"));
                result.Add(new KeyValue<string>(key: "So trang", value: $"{item.NumberOfPage}"));
                result.Add(new KeyValue<string>(key: "Ngay nhap", value: $"{item.InputDate}"));
                result.Add(new KeyValue<string>(key: "Ten tac gia", value: $"{item.AuthorOfBook.Name}"));
                result.Add(new KeyValue<string>(key: "Ten nha xuat ban", value: $"{item.PublisherOfBook.Name}"));
                result.Add(new KeyValue<string>(key: "Tinh trang sach", value: $"{item.Status}"));
            }

            return result;
        }
    }
}