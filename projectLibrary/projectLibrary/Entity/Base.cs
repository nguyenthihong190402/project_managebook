﻿using System;
using System.Collections.Generic;
using System.Text;

namespace projectLibrary.Entity
{
    class Base
    {
        protected string _name;
        protected int _id;

        public string Name { get => _name; set => _name = value; }
        public int Id { get => _id; }

        public Base(string name = "")
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new Exception("ten khong duoc de trong");
            }
            this._name = name;
      
        }

    }
}
