﻿using System;
using System.Collections.Generic;
using System.Text;
using projectLibrary.Interface;
using projectLibrary.Services;

namespace projectLibrary.Entity
{
    class Publisher : Base, IPublisher
    {
        private static List<Publisher> _publishersList;
        private List<Book> _booksList;
        private static int _autoIncreatment = 0;

        public static List<Publisher> PublisherList { get => _publishersList; }
        public List<Book> PublisherBooksList { get => _booksList; }

        /// <summary>
        /// khởi tạo 1 nhà xuất bản
        /// </summary>
        /// <param name="name">tên nhà xuất bản</param>
        public Publisher(string name = "", int?id = null) : base(name: name)
        {
            if (_publishersList == null)
            {
                _publishersList = new List<Publisher>();
            }
            base._id = id.HasValue?id.Value: ++_autoIncreatment;
            this.Name = name;
            _booksList = new List<Book>();
            _publishersList.Add(this);
        }

        /// <summary>
        /// lấy dữ liệu cần ghi vào file
        /// </summary>
        /// <returns>chuỗi dữ liệu để ghi vào file</returns>
        public static string[] ToFile()
        {
            List<string> dataOfPublisher = new List<string>();
            dataOfPublisher.Add($"{_autoIncreatment}");
            if (_publishersList != null)
            {
                for (int i = 0; i < _publishersList.Count; i++)
                {
                    dataOfPublisher.Add($"{_publishersList[i].Id}###{_publishersList[i].Name}");
                }
            }
            //add o day la add cac phan tu cua list a
            return dataOfPublisher.ToArray();
        }



        public static void WriteFile(string path)
        {
            string[] arrString = ToFile();
            IOFIle.WriteFile(path, arrString);

        }

        public static void ReadFile(string path)
        {
            string[] arr = IOFIle.ReadFile(path);
        }

    }
}

