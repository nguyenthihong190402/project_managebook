﻿using System;
using System.Collections.Generic;
using System.Text;
using projectLibrary.Interface;
using projectLibrary.Services;

namespace projectLibrary.Entity
{
    class Author : Base, IAuthor
    {
        private static List<Author> _authorsList;
        private List<Book> _booksList;
        private static int _autoIncreatment = 0;

        public static List<Author> ListAuthor { get => _authorsList; }
        public List<Book> BooksList { get => _booksList; }

        /// <summary>
        /// dùng để khỏi tạo 1 tác giả
        /// </summary>
        /// <param name="name">tên tác giả</param>
        public Author(string name ="", int? id = null) : base(name: name)
        {
            if (_authorsList == null)
            {
                _authorsList = new List<Author>();
            }
            base.Name = name;
            base._id = id.HasValue?id.Value: ++_autoIncreatment;
            this._booksList = new List<Book>();
            _authorsList.Add(this);
        }
        public static string[] ToFile()
        {
            List<string> _dataAuthor = new List<string>();
            _dataAuthor.Add($"{_autoIncreatment}");
            if (_authorsList != null)
            {
                for (int i = 0; i < _authorsList.Count; i++)
                {
                    _dataAuthor.Add($"{_authorsList[i].Id}###{_authorsList[i].Name}");

                }
            }
            //add o day la add cac phan tu cua list a
            return _dataAuthor.ToArray();
        }

        public static void WriteFile(string path)
        {
            string[] arrString = ToFile();
            IOFIle.WriteFile(path,arrString);
            
        }

        public static void ReadFile(string path)
        {
          string[]arr = IOFIle.ReadFile(path);

          int.TryParse(arr[0], out _autoIncreatment);
            Author._authorsList.Clear();
            for (int i = 1; i < arr.Length; i++)
            {
                string[] arr2 = arr[i].Split("###");
                Author._authorsList.Add(new Author(arr2[0], Convert.ToInt32(arr2[1])));
            }
           
        }



    }
}
